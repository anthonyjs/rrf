PROGRAM find_precision

!  Find the maximum available precision for integers and reals
!  for the compiler used to compile this.

IMPLICIT NONE
INTEGER :: q, qi, qp, kind, ok
CHARACTER(80) :: buffer

ok = 0
do q = 6,64
  qi = selected_int_kind(q)
  if (qi > 0) then
    ok = q
    kind = qi
  else
    if (ok > 0) then
      print "(/a,i0,a,i0)", "Integer kind ", kind, ": Decimal digits ", ok
    end if
    print "(a,i0,a)", "Digit length ", q, " not available"
    exit
  end if
end do

ok = 0
do q = 6,64
  qp = selected_real_kind(q)
  if (qp > 0) then
    ok = q
    kind = qp
  else
    if (ok > 0) then
      print "(/a,i0,a,i0)", "Real kind ", kind, ": Precision ", ok
    end if
    print "(a,i0,a)", "Precision ", q, " not available"
    exit
  end if
end do

END PROGRAM find_precision

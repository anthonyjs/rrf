SUBROUTINE f_main

!  The Fortran main program has to be a subroutine called f_main when
!  using the ARPREC package. This routine merely calls rrfcalc.

!  We probably don't need this here
USE mpmodule

call rrfcalc

END SUBROUTINE f_main

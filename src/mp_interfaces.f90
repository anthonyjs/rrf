MODULE mp_interfaces

USE mpmodule
IMPLICIT NONE

interface operator (.eq.)
  module procedure mpeqt_ji
  module procedure mpeqt_ij
end interface

interface operator (.gt.)
  module procedure mpgtt_ji
  module procedure mpgtt_ij
end interface

interface operator (.lt.)
  module procedure mpltt_ji
  module procedure mpltt_ij
end interface

interface modulo
  module procedure mp_modulo_ji
end interface

CONTAINS

LOGICAL FUNCTION mpeqt_ji(ja,ib)
TYPE (mp_integer), intent(in) :: ja
INTEGER, intent(in) :: ib
TYPE (mp_integer) :: jb
jb=ib
mpeqt_ji=mpeqt_jj(ja,jb)
END FUNCTION mpeqt_ji

LOGICAL FUNCTION mpeqt_ij(ia,jb)
TYPE (mp_integer), intent(in) :: jb
INTEGER, intent(in) :: ia
TYPE (mp_integer) :: ja
ja=ia
mpeqt_ij=mpeqt_jj(ja,jb)
END FUNCTION mpeqt_ij

LOGICAL FUNCTION mpgtt_ji(ja,ib)
TYPE (mp_integer), intent(in) :: ja
INTEGER, intent(in) :: ib
TYPE (mp_integer) :: jb
jb=ib
mpgtt_ji=mpgtt_jj(ja,jb)
END FUNCTION mpgtt_ji

LOGICAL FUNCTION mpgtt_ij(ia,jb)
TYPE (mp_integer), intent(in) :: jb
INTEGER, intent(in) :: ia
TYPE (mp_integer) :: ja
ja=ia
mpgtt_ij=mpgtt_jj(ja,jb)
END FUNCTION mpgtt_ij

LOGICAL FUNCTION mpltt_ji(ja,ib)
TYPE (mp_integer), intent(in) :: ja
INTEGER, intent(in) :: ib
TYPE (mp_integer) :: jb
jb=ib
mpltt_ji=mpltt_jj(ja,jb)
END FUNCTION mpltt_ji

LOGICAL FUNCTION mpltt_ij(ia,jb)
TYPE (mp_integer), intent(in) :: jb
INTEGER, intent(in) :: ia
TYPE (mp_integer) :: ja
ja=ia
mpltt_ij=mpltt_jj(ja,jb)
END FUNCTION mpltt_ij

TYPE(mp_integer) FUNCTION mp_modulo_ji(ja,ib)
TYPE(mp_integer), INTENT(IN) :: ja
INTEGER, INTENT(IN) :: ib
TYPE(mp_integer) :: j
j=ja/ib
j=j*ib
mp_modulo_ji=ja-j
END FUNCTION mp_modulo_ji


END MODULE mp_interfaces

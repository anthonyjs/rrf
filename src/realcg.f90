PROGRAM realcg


!  This program constructs coupling coefficients <j1k1,j2k2|JK> for
!  coupling two angular momenta or spherical tensors, expressed in
!  real components j1k1 and j2k2, into coupled components JK, also
!  in real form.


!  The program takes data from standard input:
!    WRITE prefix
!      Write the results to files with the specified prefix.
!      If no prefix is specified, the results are only printed on
!      standard output.
!    COUPLE j1 j2
!      Calculate the coupling coefficients for the specified j values,
!      print them on standard output, and if a prefix is specified,
!      write them to the file <prefix>_<j1>_<j2>.
!  E.g. the data file
!    WRITE realcg
!    COUPLE 1 2
!  write coupling coefficients between j1=1 and j2=2 to file realcg_1_2
!  These results are intended for machine-readable use. The results are
!  also printed to standard output in more juman-readable form.

!  So <prefix>_j1_j2 contains the coupling coefficients for the angular
!  momenta j1 and j2. Only the nonzero coefficients are listed. Each line
!  of the file describes one nonzero coefficient in the form
!  l1 l2 L i1 i2 i3 i4
!  where l1, l2 and L are integers indexing the angular momentum
!  components. l1 and l2 index the components of j1 and j2 respectively:
!  0  1  2  3  4  5  6  ...
!  0  1c 1s 2c 2s 3c 3s ...
!  while L indexes the coupled function JK as follows:
!  0   1   2   3   4   5   6   7   8   9  ...
!  00  10  11c 11s 20  21c 21s 22c 22s 30 ...
!  The value of the coefficient is (i1/i2)sqrt(i3/i4). These
!  coefficients may be pure imaginary, in which case i3<0. sqrt(-n)
!  always means +i*sqrt(n). The transformation matrix between the
!  uncoupled functions |j1k1,j2k2> and the coupled functions |JK> is
!  unitary. 

USE input
USE rrf_module
! USE rrfdata
! USE rrflist
! USE factorials
! USE arithmetic
USE wigner
IMPLICIT NONE

INTEGER, PARAMETER :: dp=kind(1d0)

INTEGER, PARAMETER :: BUFSIZE=400, MAXJ=10

!  Fudge to allow pointer arrays
TYPE rrfp
  TYPE(rrf), POINTER :: rrf
END TYPE rrfp

TYPE(rrfp) :: x(0:2*MAXJ,-MAXJ:MAXJ), xc(-MAXJ:MAXJ,0:2*MAXJ)
!  x is the transformation matrix between real and complex spherical tensors.
!    1st index of x runs from 0 to 2j and the second from -j to j.
!  xc is the inverse transformation matrix.

TYPE(rrfp), ALLOCATABLE :: cgc(:,:,:,:), cgr(:,:,:)
!  cgc is the matrix of standard Clebsch--Gordan coefficients for particular
!  j1 and j2, labelled by (m1,m2,J,M)
!  cgr is the matrix of Clebsch--Gordan coefficients for particular j1 and j2,
!  for tensors and functions expressed in real form.

!  m values for complex components run from -j to j.
!  k values for real components run 0, 1=1c, 2=1s, 3=2c, 4=2s, etc

!COMPLEX(KIND=dp) :: cgrd(0:8,0:8,0:81)
COMPLEX(KIND=dp), ALLOCATABLE :: cgrd(:,:,:)
!  cgrd is the matrix of Clebsch--Gordan coefficients for particular j1 and j2,
!  for tensors and functions expressed in real form, given as double-precision
!  numerical values.

LOGICAL :: eof
CHARACTER(LEN=16) :: word
CHARACTER(LEN=2) :: label(0:18)=(/ "0 ", "1c", "1s", "2c", "2s", "3c", "3s", &
    "4c", "4s", "5c", "5s", "6c", "6s", "7c", "7s", "8c", "8s", "9c", "9s"/)
CHARACTER(LEN=BUFSIZE) :: out
CHARACTER(LEN=40) :: outfile_prefix=""

INTEGER :: j1, j2, k, m, mc, ms, ok, p, t
TYPE(rrf), POINTER :: i=>null(), rthalf=>null()

call init_rrf()

call unit(rthalf)
call multi(rthalf,int(2,long),-1)
call root(rthalf)
!  Now rthalf = sqrt(1/2)
call unit(i)
call chsign(i)
call root(i)
!  Now i = sqrt(-1)

!  Construct transformation matrix
do m = -MAXJ,MAXJ
  do k = 0,2*MAXJ
    call setzero(x(k,m)%rrf)
    call setzero(xc(m,k)%rrf)
  end do
end do
call unit(x(0,0)%rrf)
do m = 1,MAXJ
  mc = 2*m-1
  ms = 2*m
  call copy(rthalf,x(mc,-m)%rrf)
  call copy(rthalf,x(ms,-m)%rrf); call mult(x(ms,-m)%rrf,i); call chsign(x(ms,-m)%rrf)
  call copy(rthalf,x(mc,m)%rrf); if (modulo(m,2) == 1) call chsign(x(mc,m)%rrf)
  call copy(rthalf,x(ms,m)%rrf); call mult(x(ms,m)%rrf,i); if (modulo(m,2) == 1) call chsign(x(ms,m)%rrf)
end do

!  Check cartesian-spherical transformation matrix
!  do k = 0,8
!    t = 1
!    out = ""
!    write (out,"(2a)") "k = ", label(k)
!    p = 8
!    do m = -4,4
!      t = p
!      call char4i(x(k,m)%rrf, out, t, BUFSIZE)
!      p = p + 15
!    end do
!    print "(a)", trim(out)
!  end do
  
!  Inverse transformation matrix

do m = -MAXJ,MAXJ
  do k = 0,2*MAXJ
    call copy(x(k,m)%rrf,xc(m,k)%rrf)
    if (nonzero(xc(m,k)%rrf)) then
      call conjugate(xc(m,k)%rrf)  !  Complex conjugate
    end if
  end do
end do

!  Check inverse cartesian-spherical transformation matrix
!  do m = -4,4
!    t = 1
!    out = ""
!    write (out,"(a,i2)") "m = ", m
!    p = 8
!    do k = 0,8
!      t = p
!      call char4i(xc(m,k)%rrf, out, t, BUFSIZE)
!      p = p + 15
!    end do
!    print "(a)", trim(out)
!  end do
  
allocate (cgc(-MAXJ:MAXJ,-MAXJ:MAXJ,0:2*MAXJ,-2*MAXJ:2*MAXJ),          &
    cgr(0:2*MAXJ,0:2*MAXJ,0:(2*MAXJ+1)**2),                            &
    cgrd(0:2*MAXJ,0:2*MAXJ,0:(2*MAXJ+1)**2), stat=ok)
if (ok>0) call die("Can't allocate cg arrays")

do
  call read_line(eof)
  if (eof) exit
  do while (item < nitems)
    call readu(word)
    select case(word)
    case("","NOTE","!")
      exit
    case("STOP","FINISH","QUIT","Q","END")
      stop
    ! case("MAXJ","MAX","MAXIMUM")
    !   call readi(maxj)
    case("EXPORT","WRITE","PREFIX")
        call reada(outfile_prefix)
    case("COUPLE")
      call readi(j1)
      call readi(j2)
      if (j1+j2>MAXJ) then
        write (out,"(a,i0)") "Maximum coupled J value is ", MAXJ
        call die (out,.true.)
      end if
      print "(a,i0,a,i0)", "Coupling ", j1, " and ", j2
      call couple(j1,j2)
    case default
      print "(3a)", "Keyword ", trim(word), " not recognised"
    end select
  end do
end do

CONTAINS

SUBROUTINE couple(j1,j2)

INTEGER, INTENT(IN) :: j1,j2
INTEGER :: i, j, m, m1, m2, k1, k2, k1p, k2p, p, t, jmin, jmax,       &
    ok, pmin, pmax
INTEGER(KIND=long) :: i1, i2, i3, i4
CHARACTER(LEN=20) :: file

TYPE(rrfp), ALLOCATABLE :: cgt1(:,:,:,:), cgt2(:,:,:,:)
TYPE(rrf), POINTER :: one => null()
TYPE(rrfp) :: sum
LOGICAL :: flag
REAL(qp) :: a

sum%rrf => null()
call unit(one)

if (.not. allocated(cgt1)) then
  allocate(cgt1(0:2*MAXJ,-MAXJ:MAXJ,0:2*MAXJ,-2*MAXJ:2*MAXJ),          &
      cgt2(0:2*MAXJ,0:2*MAXJ,0:2*MAXJ,-2*MAXJ:2*MAXJ), stat=ok)
  if (ok>0) call die("Can't allocate temporary rrf arrays")
end if

!  Construct table of standard Clebsch-Gordan coefficients

call zero4(cgc)
call zero3(cgr)

! print "(a)", "Zeroed"

jmax = j1+j2
jmin = abs(j1-j2)
do j = jmin,jmax
  do m1 = -j1,j1
    do m2 = -j2,j2
      call cg(j1,j2,j, m1,m2,m1+m2, 1, cgc(m1,m2,j,m1+m2)%rrf)
    end do
  end do
end do

print "(/a)", "Clebsch-Gordan coefficients"
print "(/a,i1,a,i1)", "j1 = ", j1, ", j2 = ", j2
do m = jmax,-jmax,-1
  out = ""
  write(out,"(a)") "m1 m2   JM:"
  p = 10
  do j = jmax,jmin,-1
    if (-j <= m .and. m <= j) then
      write (out(p+3:p+20),fmt="(i1,1x,i2,5x)") j, m
      p = p+20
    end if
  end do
  print "(/a)", trim(out)
  do m1 = j1,-j1,-1
    out = ""
    m2 = m-m1
    if (-j2 <= m2 .and. m2 <= j2) then
      write (out,fmt="(i2,1x,i2)") m1, m2
      p = 10
      do j = jmax,jmin,-1
        if (-j <= m .and. m <=  j) then
          t = p
          call char4i(cgc(m1,m2,j,m)%rrf, out, t, BUFSIZE)
          p = p+20
        end if
      end do
      print "(a)", trim(out)
    end if
  end do
end do

!  Transform

call zero4(cgt1)
call zero4(cgt2)

do j = jmin,jmax
  do m = -j,j
    ! print "(/a)", "k1   j2 m2   J  M"
    do m2 = -j2,j2
      do k1 = 0,2*j1
        ! print "(a,2i2)", "calling scalarproduct ", m2, k1
        call scalarproduct(x(k1,-j1:j1),cgc(-j1:j1,m2,j,m),              &
            cgt1(k1,m2,j,m))
        ! write (out,"(i0,a,i4,i3,i4,i3)") j1,label(k1), j2,m2, j, m
        ! p = 22
        ! call char4i(cgt1(k1,m2,j,m)%rrf, out, p, BUFSIZE)
        ! print "(a)", trim(out)
      end do
    end do
    ! print "(a)", "First scalar product"
    ! print "(/a)", "k1   k2    J  M"
    do k1 = 0,2*j1
      do k2 = 0,2*j2
        call scalarproduct(x(k2,-j2:j2),cgt1(k1,-j2:j2,j,m),             &
            cgt2(k1,k2,j,m))
        ! write (out,"(i0,a,2x,i0,a,i4,i3)") j1,label(k1), j2,label(k2), j, m
        ! p = 20
        ! call char4i(cgt2(k1,k2,j,m)%rrf, out, p, BUFSIZE)
        ! print "(a)", trim(out)
      end do
    end do
  end do
  ! print "(/a)", "k1   k2    K"
  do k = 0,2*j
    do k1 = 0,2*j1
      do k2 = 0,2*j2
        call scalarproduct(cgt2(k1,k2,j,-j:j),xc(-j:j,k),cgr(k1,k2,j**2+k))
        ! write (out,"(i0,a,2x,i0,a,2x,i0,a)")                               &
        !     j1,label(k1), j2,label(k2), J,label(k)
        ! p = 20
        ! call char4i(cgr(k1,k2,j**2+k)%rrf, out, p, BUFSIZE)
        ! print "(a)", trim(out)
      end do
    end do
  end do
end do

print "(/a)", "Coupling coefficients for real components"
print "(/a,i1,a,i1)", "j1 = ", j1, ", j2 = ", j2
cgrd = cmplx(0d0,0d0,dp)
do m = 0,jmax
  do m1 = 0,min(m,j1)
    m2 = m-m1
    if (m2>j2) cycle
    out = ""
    print "(1x)"
    write(out,"(a)") "J K  k1 k2:"
    p = 10
    do k1 = 2*m1-1,2*m1
      if (k1<0) cycle
      do k2 = 2*m2-1,2*m2
        if (k2<0) cycle
        write (out(p+3:),fmt="(a2,1x,a2)") label(k1), label(k2)
        p = p+20
      end do
    end do
    print "(a)", trim(out)

    do j = jmax,jmin,-1
      do k = 0,2*j
        flag = .false.
        out = ""
        write (out,fmt="(i1,1x,a)") j, label(k)
        p = 10
        do k1 = 2*m1-1,2*m1
          if (k1<0) cycle
          do k2 = 2*m2-1,2*m2
            if (k2<0) cycle
            if (nonzero(cgr(k1,k2,j**2+k)%rrf)) then
              t = p
              call char4i(cgr(k1,k2,j**2+k)%rrf, out, t, BUFSIZE)
              call rrf_to_real(cgr(k1,k2,j**2+k)%rrf,a,i)
              if (i==0) then
                cgrd(k1,k2,j**2+k) = cmplx(a,0d0,dp)
              else
                cgrd(k1,k2,j**2+k) = cmplx(0d0,a,dp)
              end if
              flag = .true.
            else
              write(out(p+5:),fmt="(a)") "0"
            end if
            p = p+20
          end do
        end do
        if (flag) then
          print "(a)", trim(out)
        end if
      end do
    end do
  end do
end do

!  Check unitarity
pmin = jmin**2
pmax = (jmax+1)**2-1
ok = 0
do k1 = 0,2*j1
  do k1p = 0,2*j1
    do k2 = 0,2*j2
      do k2p = 0,k2
        call scalarproduct(cgr(k1,k2,pmin:pmax),cgr(k1p,k2p,pmin:pmax),sum)
        if (k1==k1p .and. k2==k2p) then
          if (.not. equal(sum%rrf,one)) then
            print "(a,1x,2a)", label(k1), label(k2), ": column not normalized"
            ok = ok+1
          end if
        else
          if (.not. iszero(sum%rrf)) then
            print "(3a,1x,4a)", label(k1),",", label(k2),              &
                label(k1p),",", label(k2p), ": columns not orthogonal"
            ok = ok+1
          end if
        end if
      end do
    end do
  end do
end do
if (ok>0) then
  call die ("MATRIX IS NOT UNITARY")
else
  print "(//a,i0,a,i0,a/)", "j1 = ", j1, ", j2 = ", j2, ": Matrix is unitary"
end if

if (outfile_prefix .ne. "") then
  write(file,"(a,a,i0,a,i0)") trim(outfile_prefix), "_", j1, "_", j2
  open (9,file=file,recl=256,status="replace")
  do i = jmin**2,(jmax+1)**2-1
    do j = 0,2*j2
      do k = 0,2*j1
        if (.not. iszero(cgr(k,j,i)%rrf)) then
          call rrf_to_4i(cgr(k,j,i)%rrf,i1,i2,i3,i4)
          write (9,"(6(i0,1x),i0)") k, j, i, i1,i2,i3,i4
        end if
      end do
    end do
  end do
  close(9)
end if

END SUBROUTINE couple

SUBROUTINE scalarproduct(a,b,c)

TYPE(rrfp) :: a(:), b(:)
TYPE(rrfp) :: c

INTEGER :: i, tt
TYPE(rrf), POINTER :: k=>null()

call setzero(c%rrf)
call assert(size(a)==size(b),"Incompatible vectors")
do i = 1,size(a)
  call copy(a(i)%rrf,k)
  call conjugate(k)
  call mult(k,b(i)%rrf)
  call add(c%rrf,k)
  call clear(k)
end do

END SUBROUTINE scalarproduct

SUBROUTINE zero3(c)
TYPE(rrfp) :: c(:,:,:)
INTEGER :: i, j, k

do i = 1,size(c,1)
  do j = 1,size(c,2)
    do k = 1,size(c,3)
      call setzero(c(i,j,k)%rrf)
    end do
  end do
end do
END SUBROUTINE zero3

SUBROUTINE zero4(c)
TYPE(rrfp) :: c(:,:,:,:)
INTEGER :: i, j, k, l

do i = 1,size(c,1)
  do j = 1,size(c,2)
    do k = 1,size(c,3)
      do l = 1,size(c,4)
        call setzero(c(i,j,k,l)%rrf)
      end do
    end do
  end do
end do
END SUBROUTINE zero4

END PROGRAM realcg

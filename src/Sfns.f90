PROGRAM Sfns

!  Find the product of two S functions as a linear combination
!  of S functions.

!  Run the program for instructions.

!  Not fully tested

USE rrf_module
USE wigner
USE input

IMPLICIT NONE

INTEGER :: la1, la2, ja, ka1, ka2, lb1, lb2, jb, kb1, kb2, l1, l2, j,   &
    k1, k2, m, n, p, x
TYPE(rrf), POINTER :: g=>null(), g1=>null(), g2=>null(), g3=>null(),    &
    g9=>null(), ga=>null(), gb=>null(), gc=>null()
TYPE term
  TYPE(rrf), POINTER :: gz  => null()
END TYPE term
TYPE(term) :: z(20)
INTEGER :: index(5,20)
CHARACTER(LEN=1) :: c=",", sc=";", plus
CHARACTER(LEN=40) :: buffer
CHARACTER(LEN=16) :: w
LOGICAL :: eof, Ain, Bin, Sbar=.true.
!  If Sbar is true, calculate the renormalized S-bar functions

call init_rrf(quiet=.true.)

print "(a)", "This program expresses the product of two S functions", &
    "as a linear combination of single S functions."
print "(a)", "It works in terms of either the Ordinary S functions", &
    "or the Renormalized S-bar functions (default)."
print "(a)", "In either case it works with the complex components,",    &
    "not the real components 0, 1c, 1s, 2c, 2s, etc."

write (*,fmt="(/a)", advance="no") "Specify R or O: "
call read_line(eof)
call reada(w)
select case(upcase(w))
case("R")
  Sbar=.true.
case("O")
  Sbar=.false.
case("")
  Sbar=.true.
case default
  call die ("Don't understand "//trim(w),.true.)
  stop
end select

!  Note that this program uses indices ka1, ka2, kb1, kb2 internally, rather than
!  ma1, ma2, mb1, mb2, but it works with the complex S functions, not the real ones.

print "(a/a/)", "S-function indices l1, l2, j, m1, m2 must be entered on one line.", &
    "Omitted values will be read as zero."
main_loop: do
  Ain = .false.
  Bin = .false.
  do
    if (.not. Ain) then
      write (*,fmt="(a)", advance="no") "Enter l1, l2, j, m1, m2 for S-function A: "
      call read_line(eof)
      if (eof) exit
      if (nitems == 0) cycle
      call readu(w)
      if (w == "END" .or. w == "QUIT" .or. w == "Q" .or. w == "STOP") exit main_loop
      call reread(-1)
      call readi(la1)
      call readi(la2)
      call readi(ja)
      call readi(ka1)
      call readi(ka2)
      if (la1+la2 < ja .or. la1+ja < la2 .or. la2+ja < la1       &
          .or. abs(ka1) > la1 .or. abs(ka2) > la2) then
        print "(a)", "Invalid S-function indices"
        cycle
      endif
      Ain = .true.
    else if (.not. Bin) then
      write (*,fmt="(a)", advance="no") "Enter l1, l2, j, m1, m2 for S-function B: "
      call read_line(eof)
      if (nitems == 0) cycle
      call readu(w)
      if (w == "END" .or. w == "QUIT" .or. w == "Q" .or. w == "STOP") exit main_loop
      call reread(-1)
      call readi(lb1)
      call readi(lb2)
      call readi(jb)
      call readi(kb1)
      call readi(kb2)
      if (lb1+lb2 < jb .or. lb1+jb < lb2 .or. lb2+jb < lb1       &
          .or. abs(kb1) > lb1 .or. abs(kb2) > lb2) then
        print "(a)", "Invalid S-function indices"
        cycle
      endif
      Bin = .true.
    end if
    if (Ain .and. Bin) exit
  end do


  p = 0
  index = 0
  k1=-ka1-kb1
  k2=-kb1-kb2
  do l1=abs(la1-lb1),la1+lb1
    do l2=abs(la2-lb2),la2+lb2
      do j=abs(ja-jb),ja+jb,2
        x=1
        call ninej(la1,lb1,l1, la2,lb2,l2, ja,jb,j, x, g9)
        if (iszero(g9)) cycle
        call threej(la1,lb1,l1, ka1,kb1,k1, x, g1)
        call threej(la2,lb2,l2, ka2,kb2,k2, x, g2)
        call three0(ja,jb,j, g3)
        call int_to_rrf(int((2*l1+1)*(2*l2+1)*(2*j+1),long),g)
        if (mod(ka1+ka2+kb1+kb2,2) .ne. 0) call chsign(g)
        m=modulo(la1-la2-ja+lb1-lb2-jb+l1-l2-j,4)
        g%list%x=modulo(g%list%x+m,4)
        call mult(g,g9,1)
        call mult(g,g1,1)
        call mult(g,g2,1)
        call mult(g,g3,1)
        if (Sbar) then
          call three0(la1,la2,ja,ga)
          call three0(lb1,lb2,jb,gb)
          call mult(g,ga,-1)
          call mult(g,gb,-1)
          call three0(l1,l2,j,gc)
          call mult(g,gc,1)
        end if
        if (iszero(g)) then
          cycle
        else
          p = p + 1
          index(:,p) = [l1,l2,j,k1,k2]
          z(p)%gz => g
          g => null()
        end if
      end do
    end do
  end do

  print "(/10(a,i0),a)",                                                &
      "S(", la1, c,la2,c,ja,sc,ka1,c,ka2, ")*S(", lb1, c,lb2,c,jb,sc,kb1,c,kb2, ") ="
  plus = ""
  if (p == 0) then
    print "(8x,a)", "zero"
  else
    plus = " "
    do n = 1,p
      buffer=""; m=1
      l1=index(1,n); l2=index(2,n); j=index(3,n); k1=index(4,n); k2=index(5,n)
      call char4i(z(n)%gz,buffer,m,40)
      print "(5x,a,1x,a,5(i0,a),a)", plus, " S(", l1, c, l2, c, j, sc, k1, c, k2,") * ", trim(buffer)
      z(n)%gz => null()
      plus = "+"
    end do
  end if
  print "(/a)", 'Enter "Q" to exit, or'
end do main_loop

END PROGRAM Sfns

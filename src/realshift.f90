PROGRAM realshift

USE input
USE rrf_module
! USE rrfdata
! USE rrflist
! USE factorials
! USE arithmetic
USE wigner
IMPLICIT NONE

INTERFACE
  SUBROUTINE start(size,quiet)
  INTEGER, INTENT(IN), OPTIONAL :: size
  LOGICAL, INTENT(IN), OPTIONAL :: quiet
  END SUBROUTINE start
END INTERFACE

INTEGER :: x(0:8,-4:4), xc(-4:4,0:8)
!  x is the transformation matrix between real and complex spherical tensors.
!  xc is the inverse transformation matrix.

INTEGER :: cgc(-4:4,-4:4,0:8,-8:8), cgr(0:8,0:8,0:81)
!  cgc is the matrix of standard Clebsch--Gordan coefficients for particular
!  j1 and j2.
!  cgr is the matrix of Clebsch--Gordan coefficients for particular j1 and j2,
!  for tensors and functions expressed in real form.

!  m values for complex components run from -j to j.
!  k values for real components run 0, 1=1c, 2=1s, 3=2c, 4=2s, etc


LOGICAL :: eof, details=.false., debug=.false.
CHARACTER(LEN=16) :: word
CHARACTER(LEN=2) :: label(0:8)=(/ "0 ", "1c", "1s", "2c", "2s", "3c", "3s", "4c", "4s" /)
CHARACTER(LEN=150) :: out

INTEGER :: l, p, t

call start(10000,.true.)

call transform

do
  call read_line(eof)
  if (eof) exit
  do while (item < nitems)
    call readu(word)
    select case(word)
    case("","NOTE")
      exit
    case("STOP","FINISH","QUIT","END")
      stop
    case("DEBUG")
      call readu(word)
      select case(word)
      case("","ON")
        debug=.true.
      case("OFF")
        debug=.false.
      end select
    case("SHIFT")
      call readi(l)
      call shift(l)
    end select
  end do
end do

CONTAINS

SUBROUTINE shift(l)

INTEGER, INTENT(IN) :: l

INTEGER :: ll, m, mm, k, kk, kkk, lmll
LOGICAL :: zero
CHARACTER(LEN=2) :: s
INTEGER :: g=0, gg=0, ggg=0

do k=0,2*l
  write (out,"(a,i1,2a)") "Q^o_{", l, trim(label(k)), "}"
  p=len_trim(out)+1
  s=" ="
  do ll=0,l
    lmll=l-ll
    do kk=0,2*ll
      ! write (out,"(a,i1,2a,i1,2a)")                                   &
      !     "W_{", ll, trim(label(kk)), ",", l, trim(label(k)), "}(c)"
      do kkk=0,2*lmll
        g=0
        do m=-l,l
          if (abs(m) .ne. (k+1)/2) cycle
          do mm=-ll,ll
            if (abs(mm) .ne. (kk+1)/2) cycle
            if (abs(m-mm) .ne. (kkk+1)/2) cycle
            if (xc(m,k) .ne. 0 .and. x(kk,mm) .ne. 0                   &
                .and. x(kkk,m-mm) .ne. 0) then
              call copy(x(kk,mm),gg)
              call show("a",gg)
              call mult(gg,x(kkk,m-mm))
              call show("b",gg)
              call mult(gg,xc(m,k))
              call show("c",gg)
              call setf(l+m,ggg)
              call show("d",ggg)
              call multf(ggg,ll+mm,-1)
              call show("e",ggg)
              call multf(ggg,l+m-ll-mm,-1)
              call show("f",ggg)
              call multf(ggg,l-m)
              call show("g",ggg)
              call multf(ggg,ll-mm,-1)
              call show("h",ggg)
              call multf(ggg,l-m-ll+mm,-1)
              call show("i",ggg)
              call root(ggg)
              call show("j",ggg)
              call mult(gg,ggg)
              call show("k",gg)
              call add(g,gg)
              call show("l",g)
              call clear(gg)
              call clear(ggg)
            end if
          end do
        end do
        if (g .ne. 0) then
          if (p > 55) then
            print "(a)", trim(out)
            out=""
            p=12
          end if
          out(p:p+1)=s
          t=p+3
          call show("m",g)
          call char4i(g, out, t, 150)
          write (out(t:),fmt="(a,i1,3a,i1,2a)")                     &
              " R_{", lmll, trim(label(kkk)), "}(c)",               &
              " Q^c_{", ll, trim(label(kk)), "}"
          p=len_trim(out)+1
          s=" +"
        end if
      end do
    end do
  end do
  print "(a)", trim(out)
end do
              
END SUBROUTINE shift

SUBROUTINE show(string,g)

CHARACTER(LEN=*), INTENT(IN) :: string
INTEGER :: g
CHARACTER(LEN=32) :: s
INTEGER :: p

if (.not. debug) return
s=string
p=4
call char4i(g, s, p, 32)
print "(a)", trim(s)

END SUBROUTINE show

SUBROUTINE transform

INTEGER :: i=0, k=0, m, mc, ms

call unit(k)
call multi(k,2,-1)
call root(k)
!  Now k=sqrt(1/2)
call unit(i)
call chsign(i)
call root(i)
!  Now i=sqrt(-1)

!  Construct transformation matrix

x=0;
call unit(x(0,0))
do m=1,4
  mc=2*m-1
  ms=2*m
  call copy(k,x(mc,-m))
  call copy(k,x(ms,-m)); call mult(x(ms,-m),i); call chsign(x(ms,-m))
  call copy(k,x(mc,m)); if (modulo(m,2) == 1) call chsign(x(mc,m))
  call copy(k,x(ms,m)); call mult(x(ms,m),i); if (modulo(m,2) == 1) call chsign(x(ms,m))
end do
call clear(k)
call clear(i)

! do m=-3,3
!   p=1
!   do k=0,6
!     t=p
!     call char4i(x(k,m), out, t, 150)
!     p=p+12
!   end do
!   print "(a)", trim(out)
! end do

!  Inverse transformation matrix

do m=-4,4
  do k=0,8
    call copy(x(k,m),xc(m,k))
    if (xc(m,k) .ne. 0) then
      lx(xc(m,k))=modulo(4-lx(xc(m,k)),4)  !  Complex conjugate
    end if
  end do
end do

END SUBROUTINE transform

SUBROUTINE scalarproduct(a,b,c)

INTEGER :: a(:), b(:)
INTEGER, INTENT(OUT) :: c

INTEGER :: i, j, k, l, m

c=0
k=0
call assert(size(a)==size(b),"Incompatible matrices")
do i=1,size(a)
  call copy(a(i),k)
  call mult(k,b(i))
  call add(c,k)
  call clear(k)
end do

END SUBROUTINE scalarproduct

END PROGRAM realshift

PROGRAM test6j

USE rrf_module
USE wigner
IMPLICIT NONE


INTEGER :: a, b, c, d, e, f, p, x, maxj
TYPE(node), POINTER :: k
CHARACTER(LEN=30) :: args
CHARACTER(LEN=40) :: string

call init_rrf

k=>null()
maxj=10*2

!  a - f are double the actual j or m value
do a=0,maxj
  do b=0,maxj
    do d=0,maxj
      do e=abs(a-b),min(a+b,maxj),2
        do c=abs(d-e),min(d+e,maxj),2
          do f=abs(a-c),min(a+c,maxj),2
            x=2
            call sixj(a, b, c, d, e, f, x, k)
            if ( nonzero(k) ) then
              p=1
              args=""
              call put(a, args,p)
              call put(b, args,p)
              call put(c, args,p)
              call put(d, args,p)
              call put(e, args,p)
              call put(f, args,p)
              p=1
              string=""
              call char4i(k, string, p, 40)
              print "(a,2x,a)", args, trim(string)
            end if
          end do
        end do
      end do
    end do
  end do
end do


CONTAINS

SUBROUTINE put(j, s,p)

CHARACTER(LEN=*) :: s
INTEGER, INTENT(IN) :: j
INTEGER, INTENT(INOUT) :: p

if (mod(j,2) == 0) then
  write (unit=s(p:p+1),fmt="(i2)") j/2
else
  write (unit=s(p:p+3),fmt="(i2,a2)") j,"/2"
endif
p=p+5

END SUBROUTINE put

! LOGICAL FUNCTION delta(a,b,c, x)
! 
! INTEGER, INTENT(IN) :: a, b, c, x
! 
! if (a<0 .or. b<0 .or. c<0) then
!   print "(a)", "Invalid arguments"
!   stop
! end if
! delta=(a+b <= c .and. b+c <= a .and. c+a <= b)
! if (x == 1) then
!   return
! else if (x == 2) then
!   delta=delta .and. (mod(a+b+c,2) == 0)
! else
!   print "(a,i0)", "Invalid argument x = ", x
!   stop
! end if
! 
! END FUNCTION delta

END PROGRAM test6j

PROGRAM test_eq



USE rrf_module
! USE rrfdata
! USE rrflist
! USE factorials
! USE arithmetic
USE input
IMPLICIT NONE

INTERFACE
  SUBROUTINE start(quiet,primes)
  LOGICAL, INTENT(IN), OPTIONAL :: quiet
  INTEGER, INTENT(IN), OPTIONAL :: primes
  END SUBROUTINE start
END INTERFACE

TYPE(rrf), POINTER :: k1=>null(), k2=>null()
LOGICAL :: ok

call start(.true.,100000)
call unit(k1)
call list(k1)
call unit(k2)
call list(k2)

if (equal(k1,k2)) then
  print "(a)", "equal"
else
  print "(a)", "not equal"
end if

END PROGRAM test_eq

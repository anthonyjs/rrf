PROGRAM show_bug

!  Attempt to illustrate a bug in gfortran's integer(16)
!  Unsuccessful: the code seems to work.

INTEGER, PARAMETER :: long=SELECTED_INT_KIND(38)
INTEGER(KIND=long), PARAMETER :: max_long_int=huge(1_long)
INTEGER :: nprime=1000, i, j, m
INTEGER :: prime(1000)
INTEGER(long) :: f

prime(1)=2
m=1
j=1
do
  m=m+2
  do i=1,j
    if (mod(m,prime(i)) == 0) exit    ! not prime
    if (m < prime(i)*prime(i)) then  ! prime found
      j=j+1
      prime(j)=m
      ! if (j .le. 50) print "(i5,i10)", j, prime(j)
      exit
    endif
  end do
  if (j == nprime) exit
end do

do
  read *, f
  if (f<2) exit
  call factorize(f)
end do

CONTAINS

SUBROUTINE factorize(n)

INTEGER(long), INTENT(IN) :: n

INTEGER(long) :: m
INTEGER :: p
LOGICAL :: new

if (n == 0) return

m=abs(n)
if (m==1) return

do p=1,nprime
  new=.true.
  do while (mod(m,int(prime(p),long)) == 0)
    print "(i0)", prime(p)
    m=m/int(prime(p),long)
  end do
  if (m == 1) exit
  if (m - int(prime(p)*prime(p),long) < 0) then
    !  Remaining factor must be prime
    print "(i0)", m
    m=1
    exit
  end if
end do

if (m>1) then
  !  Treat remaining factor as unfactorized
  print "(a,i0)", "Unfactorized: ", m
end if

END SUBROUTINE factorize

END PROGRAM show_bug

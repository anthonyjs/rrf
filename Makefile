#  Makefile for new rrf program

#  Use
#  make DEBUG "-g"
#  for debugging. Make sure that DEBUG is unset if debugging is not required.

SHELL    = /bin/bash
BASE     = ${PWD}

ifdef DEBUG
DIR = debug
else
DIR = exe
endif

#  Select the default compiler.  See ${BASE}/${COMPILER}/Flags for compiler options.
ifndef COMPILER
  COMPILER = gfortran
endif

#  If another compiler is to be used, the arprec package, if used,  will need to be
#  recompiled using that compiler.

# COMPILER = pgf
# COMPILER = ifc
# COMPILER = ifort
# COMPILER = g95
# COMPILER = alpha
# COMPILER = sgi
# COMPILER = ifc

include ${BASE}/${COMPILER}/Flags

PASS = BASE=${BASE} DEBUG="${DEBUG}" COMPILER=${COMPILER} FRC=${FRC} DEFS="${DEFS}"

rrfcalc: ${FRC}
	cd ${COMPILER}/${DIR}; ${MAKE} -f ${BASE}/makefile_body rrfcalc ${PASS}
	ln -sf ${BASE}/${COMPILER}/${DIR}/rrfcalc bin

C6coeffs Sfns realcg: modules force
	cd ${COMPILER}/${DIR}; ${MAKE} -f ${BASE}/makefile_body ${PASS} $@

test:	rrfcalc
	cd tests; ../bin/rrfcalc < test.data

test3j test6j test9j: modules
	cd ${COMPILER}/${DIR}; ${MAKE} -f ${BASE}/makefile_body ${PASS} $@
	cd tests; ../bin/$@ < $@.data > $@.out

modules: force
	cd ${COMPILER}/${DIR}; ${MAKE} -f ${BASE}/makefile_body ${PASS} $@

rebuild:
	make clean
	make rrfcalc FRC=force

clean:
	cd ${COMPILER}/${DIR}; rm -f *.o *.${MOD} ${TEMPFILES} core modules

distrib: force
	cd distrib; ${MAKE} tarfile

precision:
	@ $(COMPILER) src/find_precision.f90 && a.out
	@ rm -f a.out

force:


#-------------------------------------------

depend: force
	cd ${COMPILER}; depend -e .${MOD} ${UC} ${BASE}/src/*90 -o Dependencies

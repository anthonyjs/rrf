#   Basic root-rational-fraction module

arrow = "^"
space = " "
dot   = "."
plus  = "+"
minus = "-"
uci   = "I"
lci   = "i"
slash = "/"
star  = "*"
shriek = "!"
bra   = "("
ket   = ")"
digit = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']

class node:
  self.x = 0
  self.prime = -1
  self.next = None
  def __init__(self):
    if free:
      self = free
      free = free.next()
    else:
      self.x = 0
      self.prime = 0
      self.next = None
  
  def getdata(self):
    return (self.prime,self.x)
  def getnext(self):
    return(self.next)
  def setdata(newprime,newx):
    self.x = newx
    self.prime = newprime
  def setnext(self,newnext):
    self.next = newnext


#  Pointer to list of free nodes
free = None

class rrf:
  self.head = None
  self.factor = 1
  def __init__(self):
    self.head = node(-1,0)
    self.head.next = self.head
  def self.clear():
    j = self.head
    if free:
      i = free
      free = j.getnext()
      j.setnext(i)
      i = None
    else:
      free = j.getnext()
      j.setnext(None)
    self.head = None
    self.factor = 0
    self.setnext(None)


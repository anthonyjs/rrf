%  -*-  coding:  utf-8  -*-
\documentclass[12pt]{paper}

\usepackage{natbib}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage{braket}
\usepackage[T1]{fontenc}
\parindent 0pt
\parskip 6pt
\bibliographystyle{timf}
\newcommand\code[1]{\texttt{#1}}
\newcommand\ul{\verb:_:}

\begin{document}

\begin{center}
\textbf{\large Root Rational Fraction program, version 4.2}

A calculator program and subroutine library for working with
numbers that are square roots of rational fractions

Copyright \copyright 2003--2018 Anthony J. Stone
\end{center}


This program was originally written by Anthony Stone and Charles
Wood, and is intended primarily for the calculation of Wigner 3j, 6j
and 9j symbols, which occur in the theory of angular momentum, and for
performing elementary arithmetic with them. Further details of the
original Fortran77 implementation may be found in \cite{StoneW80}.
The current Fortran95 source code is available at
www-stone.ch.cam.ac.uk/pub/rrf-4.2.tar.gz as a compressed
tarfile (about 25 kbytes) or on the github
at https://git.uis.cam.ac.uk/x/ch-stone/u/ajs1/rrf.git. As well as
providing a stand-alone 
calculator program, it provides a library of
Fortran 90 subroutines that manipulate numbers which can be
represented as square roots of rational fractions ---
root-rational-fraction or RRF objects.

Version 4 of the program comprises Fortran modules \verb:rrf_module:
and \verb:wigner:, together with a program \verb:rrfcalc: that reads
input a line at a time and treats it as commands for a calculator
working in Reverse Polish Notation. It is convenient for interactive
use, but it can be used non-interactively by supplying a file of
commands as standard input. See \S\ref{sec:rrfcalc} for details. The
modules can be used to build other programs that need to manipulate
Wigner coefficients or other RRF objects.



\section{RRF calculator program} \label{sec:rrfcalc}

The program \texttt{rrfcalc} is designed to simplify quantum mechanics
calculations involving angular momenta, which usually involve
Clebsch--Gordan coefficients or Wigner 3j, 6j or 9j coefficients.
The 3j coefficients for low angular momentum values are tabulated in
various places but often in inconvenient forms, while 6j tabulations
are limited and harder to find, and the huge number of 9j symbols
makes tabulation impractical. Calculation of 6j and 9j coefficients from the
standard formulae in terms of 3j coefficients is tedious and
error-prone. The \texttt{rrfcalc} program 
provides the values directly and allows straightforward computation of
quite complicated formulae to give an exact numerical result.

\texttt{rrfcalc} operates in Reverse Polish Notation, which may be
unfamiliar but is quite easy to use. It deals with a stack of numbers,
initially empty. Each operation either puts a new number on the top of
the stack, or operates in some way on the top number, for example by
modifying it or displaying it, or combines the top two numbers in some
way and replaces them by the result. It is normally used
interactively, but can be used non-interactively by supplying a file
of commands as standard input.

After each line of input has been processed, the top
item on the stack is displayed, unless the last command already
displayed it. The commands available are:


\begin{tabular}{lp{122mm}}
\phantom{\texttt{0J, 3J, 6J, 9J}} & \\[-10pt]
\emph{integer} &
read the integer (which must be non-negative) and put it on the stack. To
get a negative number on the stack, you need to read in a positive number
and use CHS to change the sign. \\
\texttt{PP} \emph{rrf}  &
read the rest of the line as a root-rational-fraction in
power-of-prime form (See Appendix A), and put it on the stack. \\
+  &
Add: replace the top two elements on the stack by their sum. \\
% \end{tabular}
% 
% \begin{tabular}{lp{122mm}}
-  &
Subtract (top-of-stack from the one below).\\
*  &
Multiply \\
/  &
Divide (top-of-stack into the one below) \\
\texttt{POWER} $n$  &
raise to power $n$ ($n$ = integer or integer/2). \\
\texttt{SQRT}  &
= \texttt{POWER 1/2}. Note that it is an error to take the square root of a number
which already contains half-odd-integer powers. \\
\texttt{CHS}  &
change the sign of top-of-stack \\
\texttt{SWOP, SWAP}  &
exchange the top two items on the stack. \\
\texttt{STO} $n$  &
Store top-of-stack in memory $n$ (1 < $n$ < 25).
The item is not removed from the stack. \\
\texttt{RCL} $n$  &
Recall from memory $n$ to top-of-stack. \\
\texttt{V}  &
Verify (display) top-of-stack. \\
\texttt{LIST}  &
List the entire stack using the current default verification mode.
Top-of-stack is shown first. \\
\texttt{VP}  &
Verify top-of-stack in power-of-prime notation \\
\texttt{VI}  &
Verify top-of-stack as $(a/b)$*sqrt$(c/d)$,
where $a$, $b$, $c$ and $d$ are integers (standard
default). \\
\texttt{VF}  &
Verify top-of-stack as a floating-point number. \\
\texttt{VMP}  &
Change default verification mode to power-of-prime. \\
\texttt{VMI}  &
Change default verification mode to 
$(a/b)$*sqrt$(c/d)$. This is the usual default. \\
\texttt{VMF}  &
Change default verification mode to floating-point. \\
\texttt{0J, 3J, 6J, 9J}  &
Calculate a Wigner $3j$, $6j$ or $9j$ symbol and put it on the stack. See below. \\
\texttt{CG}  &
Calculate a Clebsch--Gordan coefficient and put it on the stack. See below.\\
\texttt{POP}  &
Delete the top member from the stack. \\
\texttt{PUSH}  &
Push the stack down, copying the top member. PUSHing an empty
stack inserts zero. \\
\texttt{CLR, CLEAR}  &
Clear the stack. \\
\texttt{PROMPT} \emph{string}  &
Set prompt string. The string is output when the program is ready
for the next line of commands. The default string is `:'.
A null string may be set. \\
\texttt{ECHO+}  &
Reflect each input line before obeying any commands. \\
\texttt{ECHO-}  &
Do not reflect input lines (default). \\
\texttt{(}  &
Ignore rest of line. \\
\texttt{?}  &
Give a summary of the available commands. \\
\texttt{QUIT, Q}  &
Exit the program. \\
\end{tabular}

The full input
for the \texttt{0J, 3J, 6J, 9J} and \texttt{CG} commands takes the form: \\
\qquad \texttt{0J} $j_1\ j_2\ j_3$ \\
\qquad \texttt{3J} $j_1\ j_2\ j_3\ m_1\ m_2\ m_3$ \\
\qquad \texttt{6J} $j_1\ j_2\ j_3\ l_1\ l_2\ l_3$ \\
\qquad \texttt{9J} $a\ b\ c\ d\ e\ f\ g\ h\ i$ \\
\qquad \texttt{CG} $j_1\ j_2\ J\ m_1\ m_2\ M$ \\
where each argument is integer or integer/2 (integers only for \texttt{0J})
and all arguments must appear on the same line separated by one or
more spaces. \texttt{0J} calculates a $3j$ symbol for which all $m$
values are zero. \code{CG} calculates the Clebsch--Gordan coefficient
$\braket{j_1\ j_2\ m_1\ m_2 | J\ M}$.


\section{RRF routine library}

The RRF calculator is suitable for simple one-off calculations, but
some calculations need a special program. The RRF routine library is
used by the calculator in the background, and is available for general use.

\begin{figure}
  \begin{center}
  \includegraphics[scale=0.5,trim=0 0 0 0]{rrf_figure.pdf}
  \caption{Structure of a root-rational-fraction (RRF) number.}
  \label{fig:rrf}
      \end{center}
\end{figure}

Each RRF (root-rational-fraction) is referred to via a Fortran
variable of \texttt{type(rrf)}. Such a variable contains a pointer to a
\code{type(node)} variable \code{list}, which points to the head of a
circular list in which the RRF is held in power-of-prime form.
(See Figure~\ref{fig:rrf}.) Each node in the list describes a factor
$(p_n)^{x_n}$, where $p_n$ is a prime number and $x_n$ is an integer or
half-odd-integer. The list only includes primes $p_n$ for which the
$x_n$ is non-zero. The node actually stores the integer $2x_n$. The
head node contains the `prime' $-1$, and its integer and
half-odd-integer powers provide sign factors $\pm1$ and $\pm i$.
The RRF also contains an integer \code{factor}
which may hold an intermediate multiplying factor that has not yet been factorized
into a product of primes. Such intermediate numbers can get extremely
large in complicated manipulations, so it is recommended to use the
gfortran compiler, which provides 16-byte integers (up to 38 decimal
digits). Other compilers, limited to 8-byte integers (18 decimal
digits) will however be adequate for normal use. The program checks
for integer overflow and should stop rather than give erroneous results.

All arithmetic and i/o manipulations are carried out by special
routines provided in the rrf module, and the list elements are not
available directly to a program that uses \code{type(rrf)}
variables. Mutiplication of RRFs is straightforward --- essentially
just a matter of merging the power-of-prime lists, adding the $x_n$
values for each prime $p_n$. Addition is much more complicated,
because each RRF has to be expressed as a power-of-prime list
describing the highest common factor of the RRFs in the sum, each
multiplied by an integer factor. These integer factors are then added
together, factorized into a power-of-prime list, and multiplied by the
common factor. When several RRFs are added sequentially, the
intermediate factors can get very large, and include very large prime
factors, but the final result in angular-momentum calculations
contains only relatively small prime factors, usually no larger than
the sum of all the angular momenta involved plus 1.

Further details of the program, in its original Fortran 77 form,
may be found in \cite{StoneW80}. 
The current version of the program comprises a Fortran module
\texttt{rrf\_module.F90} containing all the basic routines, a module
\texttt{wigner.F90}
  containing routines to calculate 3j, 6j and 9j coefficients, the
  calculator program \texttt{rrfcalc.F90}, and programs
  \texttt{test3j.f90},
  \texttt{test6j.f90}
  and \texttt{test9j.f90} which carry out various tests of the Wigner routines.
Also included are the programs \texttt{realcg.F90}, which generates
tables of coupling coefficients, analogous to Clebsch--Gordan
coefficients, for coupling spherical tensors expressed in real form
(see \citet{timf}),
and \texttt{C6coeffs.F90}, which generates Fortran code for calculating
dispersion coefficients (up to $C_{12}$, not just $C_6$) from
local distributed polarizabilities at imaginary frequency.


%  \section{Index of subroutines}
%  \subsection{Initialization function}
%  
%  \newcommand\xxx{\hfill}
%  INIT\_RRF\xxx\pageref{start}
%  
%  \subsection{Housekeeping}\label{sec:housekeeping}
%  
%  CLEAR\xxx\pageref{clear}
%  COPY\xxx\pageref{copy}
%  RENAME\xxx\pageref{rename}
%  EXCH\xxx\pageref{exch}
%  UNIT\xxx\pageref{unit}
%  ISZERO\xxx\pageref{iszero}
%  NONZERO\xxx\pageref{nonzero}
%  CHSIGN\xxx\pageref{chsign}
%  CONJUGATE\xxx\pageref{conjg}
%  
%  \subsection{Arithmetic and Conversion}\label{sec:arithmetic}
%  
%  
%  MULT\xxx\pageref{mult}
%  MULTI\xxx\pageref{multi}
%  SETF\xxx\pageref{setf}
%  MULTF\xxx\pageref{multf}
%  POWER\xxx\pageref{power}
%  ADD\xxx\pageref{add}
%  ACCUM\xxx\pageref{accum}
%  SUBTR\xxx\pageref{subtr}
%  EQUAL</a>\xxx\pageref{equal}
%  ROOT\xxx\pageref{root}
%  INT\_TO\_RRF\xxx\pageref{fromi}
%  PP\_TO\_RRF\xxx\pageref{fromch}
%  DISPLAY\xxx\pageref{display}
%  RRF\_TO\_4I\xxx\pageref{to4i}
%  CHAR4I\xxx\pageref{char4i}
%  WRITE4I\xxx\pageref{write4i}
%  RRF\_TO\_REAL\xxx\pageref{toreal}
%  RRF\_TO\_CHAR\xxx\pageref{tochar}
%  LIST\xxx\pageref{list}
%  
%  \subsection{Angular Momentum Functions}\label{sec:angular}
%  
%  THREEJ\xxx\pageref{threej}
%  SIXJ\xxx\pageref{sixj}
%  NINEJ\xxx\pageref{ninej}
%  THREE0\xxx\pageref{three0}
%  REGGE3\xxx\pageref{regge3}
%  
%  \subsection{Error handling}\label{sec:errors}
%  \subsection{Notes}\label{sec:notes}

\section{RRF library routines}

\subsection{Initialization routine}\label{Initialization}

\verb:subroutine init_rrf([quiet[,primes]]):\label{start}

This must be called before any of the other routines. It sets up a
  table of primes and initializes the table of factorials. \code{quiet} is an
  optional logical 
argument, which if true suppresses the initial program banner.
\code{primes} is an optional integer argument, which if present specifies the
number of primes to be generated in the table of primes. Default 20000. 



\subsection{Housekeeping routines}\label{housekeeping}


\code{subroutine clear(k)}\label{clear}

Deallocate RRF \code{k}. This should always be used to
release the space occupied by temporary variables before exit from
subroutines -- otherwise the list space will rapidly become full.
There is no automatic garbage collection. 

\code{subroutine copy(k1,k2)} \label{copy}

Set RRF \code{k2} equal to \code{k1}. The Fortran assignment statement \code{k2 = k1} can
  also be used; it is overloaded to do the right thing. 

\code{subroutine rename(k1,k2)} \label{rename}

Set RRF \code{k2} equal to \code{k1} and clear \code{k1}. 

\code{subroutine exch(k1,k2)} \label{exch}

Exchange the RRFs \code{k1} and \code{k2}. Use exch or rename rather than copy
where possible. 

\code{subroutine unit(k)} \label{unit}

Set \code{k} to the RRF representation of unity (not the same as $k=1$). 

\code{function iszero(k)} \label{iszero}

Returns the value .true. if RRF \code{k} represents zero and .false.
otherwise. 

\code{function nonzero(k)} \label{nonzero}

Returns the value .true. if \code{k} represents a nonzero value and
.false. otherwise. 

\code{subroutine chsign(k)} \label{chsign}

Change the sign of \code{k} (multiply by $-1$). 

\code{subroutine conjugate(k)} \label{conjg}

Replace \code{k} by its complex conjugate. 

\subsection{Arithmetic routines}\label{arithmetic}


\code{subroutine mult(k1,k2,m)} \label{mult}

Multiply \code{k1} by \code{k2**m}. \code{k1} and \code{k2} must be
different variables.  Division is achieved by having $m < 0$. 

\code{subroutine multi(k1,i,m)} \label{multi}

Multiply RRF \code{k1} by \code{i**m}, i, m integers. 

\code{subroutine setf(n,k)} \label{setf}

Set RRF k to the value of $n!$. The factorial table, which
initially contains only $0!$ and $1!$, is extended if necessary.  

\code{subroutine multf(k,n,m)} \label{multf}

Multiply RRF k by $(n!)^m$. The factorial table is extended if necessary. 

\code{subroutine power(k1,m)} \label{power}

Replace \code{k1} by \code{k1**m}. 

\code{subroutine add(k1,k2)} \label{add}

Add \code{k2} to \code{k1}, leaving \code{k2} unchanged. \code{k1} and
\code{k2} must be different RRF
variables. An additional restriction, which is in practice not a limitation
at all, is that the result of the addition must itself be
expressible as an RRF. Thus, for example, an attempt to add $\sqrt{3}$ to
$\sqrt{5}$ will provoke an error message. See Note 2. 

\code{subroutine subtr(k1,k2)} \label{subtr}

Subtract \code{k2} from \code{k1}. 

\code{logical function equal(k1,k2)} \label{equal}

The logical function \code{equal} has the value \code{.true.} if \code{k1} and \code{k2}
represent the same number, \code{.false.} otherwise. The form \code{k1 == k2}
can also be used when \code{k1} and \code{k2} are both RRFs, as can
\code{k1 /= k2} to test for inequality.

\code{subroutine root(k)} \label{root}

Replace \code{k} by its square root. This will provoke an error if
there are any primes with half-odd-integer exponents in \code{k} already.


\subsection{Conversion routines}\label{io}

These routines convert RRFs to ordinary Fortran variables, or to values
in an A1 character buffer, and vice versa, or print out a character
representation of an RRF.

\verb:subroutine int_to_rrf(i,k): \label{fromi}

Express integer I as RRF K. 

\verb:subroutine pp_to_rrf(m,n, np, k): \label{fromch}

Read an RRF from the first N characters of the character buffer
variable M. The detailed syntax of the RRF representation is given in
Appendix A.

\verb:subroutine rrf_to_4i(k, i1,i2,i3,i4): \label{to4i}

Express RRF k as (\code{i1}/\code{i2})sqrt(\code{i3}/\code{i4}).
\code{i1} and \code{i3} may be negative. \code{i2} and \code{i4} 
are always positive, unless integer overflow occurs, in which case zero
is returned in \code{i2}, and \code{i1}, \code{i3} and \code{i4}
contain rubbish.

\code{subroutine char4i(k, m,m1,m2)} \label{char4i}

Convert the rrf K to 4-integer form as characters in the buffer M of
length M2, starting at position \code{m1}. \code{m1} is updated to
point at the next available position in the buffer. If integer
overflow occurs, 12 asterisks are returned.

\verb:subroutine rrf_to_real(k,a,n): \label{toreal}

Express RRF \code{k} as \code{a*i**n}, where \code{a} is double
  precision and the integer \code{n} has the value 0 or 1 according as
  \code{k} is real or imaginary. 

\verb:subroutine rrf_to_char(k, m, m1,max, np): \label{tochar}

Express RRF \code{k} in A1 character form in the buffer \code{m(max)},
starting at position \code{m1}. The sign term is output as \code{+},
\code{-}, \code{+i} or \code{-i}; the exponents of the first \code{NP}
primes follow, and further terms appear as `. \emph{prime\^exp}'.
\code{NP} may be zero. A value of zero, +1 or -1 is output as
\code{0}, \code{+1} or \code{-1} in the sign position. An error
message is printed if there is not enough space in the buffer.
\code{m1} is updated to point at the next available position in the
buffer.

%  subroutine list(k) \label{list}
%  
%  This is a debugging aid. It prints the actual list entries for RRF
%  K on unit 6. If it is called with a negative argument -N, it prints
%  the last N list entries. Each entry occupies three integer locations;
%  the first contains a pointer to the next entry, the second contains a
%  prime, and the third is twice the exponent of that prime. The primes
%  occur in increasing order, and the last entry points back to the
%  first, for which the `prime' is always -1. `Primes' larger than the
%  square of the largest entry in the prime table may in fact be
%  composite, but this will not affect the working of the program. 



\subsection{Angular momentum functions (module wigner)}\label{angular}

\code{subroutine threej(j1,j2,j3, m1,m2,m3, x, k)} \label{threej}\\
\code{subroutine sixj(j1,j2,j3, l1,l2,l3, x, k)} \label{sixj}\\
\code{subroutine ninej(a,b,c, d,e,f, g,h,i, x, k)} \label{ninej}

These routines set the appropriate vector coupling coefficient in the RRF
\code{k}. \code{x} is a Fortran integer which should have the value 1 or 2; every
angular momentum argument
(\code{j1} etc.) is interpreted as $j_1$ or $j_1/2$ according as x = 1 or 2. 

\code{subroutine three0(j1,j2,j3, k)} \label{three0}

A more efficient equivalent of \code{threej(j1,j2,j3, 0,0,0, 1, k)}. 

\code{subroutine regge3(jp1,jp2,jp3, jm1,jm2,jm3, k)} \label{regge3} 

An alternative way to get a 3j coefficient. \code{jp1} $=j_1+m_1$,
\code{jm1} $=j_1-m_1$, etc., so all
arguments are integers even if the quantum numbers are not. Since the 3-j
routine needs $j1+m1$ etc. anyway, this is actually a more efficient way to
define the coefficient required. 

\code{subroutine CG(j1,j2,J, m1,m2,M, x, k)} \label{cg}

Set the RRF \code{k} to the Clebsch--Gordan coefficient
$\braket{ j_1 j_2 m_1 m_2 | J M }$.

\subsection{Error handling}\label{errors}

There is a variable \verb:hard_errors: in \code{rrf\_module}  which
is initially set true. With this setting most errors cause the program
to stop after printing an error message. If it is set false, errors
are soft, and cause the variable \code{error} in \verb:rrf_module:
to be set true after printing the error message. If another
error occurs while \code{error} is true, the program will
treat it as a hard error even if the variable \verb:hard_errors: 
is false. If \code{error} is set false after an error, the
program will continue, but results should be treated with suspicion.



\subsection{Notes}\label{notes}
\begin{itemize}
\item The program should not be used unless exact results are essential, as is
it inevitably much slower than ordinary arithmetic. In particular, addition
may be very slow indeed --- much slower than multiplication. 
\item The embargo on adding numbers if the result cannot be expressed as an RRF
will seem a very severe restriction. In fact, the program has been used
for several fairly elaborate calculations, involving heavy use of 3j, 6j
and 9j coefficients, without ever coming up against the restriction. I would
be interested to learn of a serious problem in which it is an important
restriction. It could be removed in principle, but would involve a complete
rewrite of the program. 
\item Machine-dependent values are set in \verb:rrf_module.F90:, and
should be checked before compiling the program. 
\item The angular momentum routines all use factorials. The largest available
value is 200!, which should suffice for most purposes. An error
message is printed if this limit is exceeded, in which case it is necessary
to recompile with a larger value for the parameter \code{MAXFCT} in \verb:rrf_module.F90:.
\item The program has been fairly thoroughly tested and has been used
successfully on a number of problems, but some bugs doubtless remain.
I should appreciate being told about them.  
\end{itemize}


\appendix

\section{Power of Prime input and output}

Numbers to be read in power-of-prime form must be written as\\
\emph{sign} $x(2)\ x(3)\ x(5)\ \dots\ x(P_{\text{NP}})$
  [\emph{prime}[\verb:^:\emph{exp}] $\mid$ \code{n!}[\verb:^:\emph{exp}] ] \dots

The sign factor must be present (even for positive numbers). It is followed
by exponents for the first \code{NP} primes; \code{NP} is
specified as an argument to the \code{PP\_TO\_RRF} routine, and may be zero. The 
remaining elements of this expression are optional, as indicated by the
square brackets (which should not themselves appear). Each element represents
a factor in the required number; that is, the elements are multiplied together
to arrive at the result. Numbers output in power-of-prime form appear in
the same format. Examples are given below.

\emph{sign} is either 0 (denoting a zero value) or \code{+},
[\code{+}]\code{1}, [\code{+}]\code{i}, \code{-}[\code{1}], or \code{-i}. The sign
term of a positive number may not be omitted. 

\emph{prime}\verb:^:\emph{exp} represents a factor comprising the specified prime
number \emph{prime} raised to the power \emph{exp}. E.g. \verb:5^2:
represents the factor 25. The exponent may be omitted if it is 1. 

\code{n!\^}\emph{exp} is read as $n!$ raised to the exponent given. The
exponent may be omitted if it is 1. 

Prime and factorial terms may occur in any order and may be interspersed
with each other. Neither may contain embedded blanks, but they are separated
by blanks or period, `.'. The` \verb:^:' introducing an exponent may be omitted if
the first character of the exponent is \code{+} or \code{-}. Exponents have the form
\code{[+|-]nnn..[/2]}.

\subsection{Examples}
\label{examples}
Using \code{NP = 0}, the line\\
\quad\verb:+ 2^-1/2 . 3^1/2 . 5+1/2 . 7-1/2 . 4!:\\
would be read as $24 \sqrt{15/14}$. Using \code{NP = 4} the same number
could be input as\\
\quad\verb:+ -1/2 1/2 1/2 -1/2 4!:\\
or as\\
\quad\verb:+ 5/2 3/2 1/2 -1/2:


\bibliography{macros,all}
\end{document}
